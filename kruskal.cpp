#include "pch.h"
#include "kruskal.h"

void printB(Edge arr[], int n)
{
	for (int i = 1; i < n; ++i)
		std::cout << arr[i].src << "   ---   " << arr[i].waga << "   ---  " << arr[i].dst << "\n";
}

Edge* kruskal(Graf *g)
{
	// liczba wierzcho�k�w
	int V = g->wierzcholki.size;
	// liczba kraw�dzi
	int E = g->krawedzie.size;
	// tablica kraw�dzi zawierajaca MST
	Edge *result = new Edge[V];  
	// Kopiec z kt�rego b�d� usuwane kolejne kraw�dzie
	Kopiec k(E);
	
	int e = 0;  
	int i = 0; 

	// Tablica kolejnych klastr�w
	Klaster *klastry = new Klaster[V];

	// do ka�dego klastra wpisany jest nr wierzcho�ka i ranga 0 
	for (int v = 0; v < V; ++v)
	{
		klastry[v].parent = v;
		klastry[v].rank = 0;
	}

	// Wpisanie wrzystkich kraw�dzi do kopca
	g->krawedzie.first();
	while (g->krawedzie.wsk != nullptr)
	{
		k.insertKey(g->krawedzie.get());
		g->krawedzie.nextNode();

	}

	while (e < V - 1)
	{
		// usuwamy najmniejsz� kraw�d� 
		Edge next_edge = k.extractMin();

		// zwracamy prarent dla dw�ch wierzcho�k�w kraw�dzi
		int x = find(klastry, next_edge.src);
		int y = find(klastry, next_edge.dst);

		// je�li x i y s� r�ne, wpisujemy kraw�d� to tablicy wynik�w i ��czymy klastry
		if (x != y)
		{
			result[e] = next_edge;
			e++;
			merge(klastry, x, y);
		}
	}
	printB(result, e);
	return result;
}


Edge* kruskal(GrafM *g)
{
	// liczba wierzcho�k�w
	int V = g->wierzcholki.size;
	// liczba kraw�dzi
	int E = g->krawedzie.size;
	// tablica kraw�dzi zawierajaca MST
	Edge *result = new Edge[V];
	// Kopiec z kt�rego b�d� usuwane kolejne kraw�dzie
	Kopiec k(E);

	int e = 0;
	int i = 0;

	// Tablica kolejnych klastr�w
	Klaster *klastry = new Klaster[V];

	// do ka�dego klastra wpisany jest nr wierzcho�ka i ranga 0 
	for (int v = 0; v < V; ++v)
	{
		klastry[v].parent = v;
		klastry[v].rank = 0;
	}

	// Wpisanie wrzystkich kraw�dzi do kopca
	g->krawedzie.first();
	while (g->krawedzie.wsk != nullptr)
	{
		k.insertKey(g->krawedzie.get());
		g->krawedzie.nextNode();

	}

	while (k.isEmpty()==false)
	{
		// usuwamy najmniejsz� kraw�d� 
		Edge next_edge = k.extractMin();

		// zwracamy prarent dla dw�ch wierzcho�k�w kraw�dzi
		int x = find(klastry, next_edge.src);
		int y = find(klastry, next_edge.dst);

		// je�li x i y s� r�ne, wpisujemy kraw�d� to tablicy wynik�w i ��czymy klastry
		if (x != y)
		{
			result[e++] = next_edge;
			merge(klastry, x, y);
		}
	}
	return result;
}


int find(Klaster k[], int i)
{
	if (k[i].parent != i)
	{
		k[i].parent = find(k, k[i].parent);
	}
	return k[i].parent;
}

void link(Klaster k[], int x, int y)
{
	if (k[x].rank < k[y].rank)
		k[x].parent = y;
	else if (k[x].rank > k[y].rank)
		k[y].parent = x;

	else
	{
		k[y].parent = x;
		k[x].rank++;
	}
}

void merge(Klaster k[], int x, int y)
{
	link(k, find(k, x), find(k, y));
}
