#include "pch.h"
#include "Dijkstra.h"


// Wypisanie wyniku
void printD(int odleglosc[], int wierzcholki[], int n, int s)
{
	for (int i = 0; i < n; i++)
	{
		std::cout << i;
		std::cout << "     [" << s << " ";
		printPath(wierzcholki, i);
		std::cout << "]     ";
		std::cout << odleglosc[i] << "\n";
	}
}

// wypisanie ścieżki
void printPath(int wierzcholki[], int j)
{
	if (wierzcholki[j] == -1)
		return;

	printPath(wierzcholki, wierzcholki[j]);

	std::cout << j << " ";
}

void Dijkstra(Graf* graph, int s)
{
	int V = graph->V;
	// tablica do zapisywania ścieżek
	int *wierzcholki = new int[V];
	// tablica do zapisywania sumy odległości
	int *odleglosc = new int[V];

	// Kopiec do przetrzymywania odległości między wierzchołkami
	KopiecP minHeap(V);

	// na początku odległości dla wszystkich wierzchołków są MAX 
	for (int v = 0; v < V; ++v) {
		wierzcholki[v] = -1;
		odleglosc[v] = INT_MAX;
		minHeap.insert(v, odleglosc[v]);
	}

	// element kopca z indeksem źródła ma odległość 0, żeby zostać usuniętym jako pierwszym
	minHeap.tablica[s] = newNode(s, odleglosc[s]);
	minHeap.pos[s] = s;
	odleglosc[s] = 0;
	minHeap.decreaseKey(s, odleglosc[s]);

	// dopóki kopiec nie będzie pusty
	while (!minHeap.isEmpty()) {
		// Usuwanie wierzchołka o najmniejszym kluczu
		kopiecNode* minHeapNode = minHeap.extractMin();
		int u = minHeapNode->v;

		// Przejście prze listę sąsiedztwa tego wierzchołka
		graph->listaSasiedztwa[u].first();
		while (graph->listaSasiedztwa[u].wsk != nullptr) {
			int v;
			if (graph->listaSasiedztwa[u].get()->dst == u)
				v = graph->listaSasiedztwa[u].get()->src;
			else
				v = graph->listaSasiedztwa[u].get()->dst;

			if (minHeap.areYouHere(v) && odleglosc[u] != INT_MAX && (graph->listaSasiedztwa[u].get()->waga + odleglosc[u]) < odleglosc[v])
			{
				odleglosc[v] = graph->listaSasiedztwa[u].get()->waga + odleglosc[u];
				wierzcholki[v] = u;
				minHeap.decreaseKey(v, odleglosc[v]);
			}
			graph->listaSasiedztwa[u].nextNode();
		}
	}

	printD(odleglosc, wierzcholki, V, s);
}

void Dijkstra(GrafM* graph, int s)
{
	int V = graph->V;
	// tablica do zapisywania ścieżek
	int *wierzcholki = new int[V];
	// tablicy do zapisywania odległości
	int *odleglosc = new int[V];

	// Kopiec do przetrzymywania odległości między wierzchołkami
	KopiecP kopiec(V);

	// na początku wszystkie odległości są ustawione na MAX
	for (int v = 0; v < V; v++) {
		wierzcholki[v] = -1;
		odleglosc[v] = INT_MAX;
		kopiec.insert(v, odleglosc[v]);
	}

	// Źródła ma odległość 0, zostanie usunięty jako pierwszy 
	kopiec.tablica[s] = newNode(s, odleglosc[s]);
	kopiec.pos[s] = s;
	odleglosc[s] = 0;
	kopiec.decreaseKey(s, odleglosc[s]);

	// dopóki kopiec nie będzie pusty
	while (!kopiec.isEmpty()) {
		// Usuwanie wierzchołka o najmniejszym kluczu
		kopiecNode* minNode = kopiec.extractMin();
		int u = minNode->v;

		// przejście przez wierzchołki sąsiadujące do v
		for (int i = 0; i < V; i++)
		{
			if (graph->macierzSasiedztwa[u][i] != nullptr)
			{
				int v;
				if (graph->macierzSasiedztwa[u][i]->dst == u)
					v = graph->macierzSasiedztwa[u][i]->src;
				else
					v = graph->macierzSasiedztwa[u][i]->dst;

				if (kopiec.areYouHere(v) && odleglosc[u] != INT_MAX && graph->macierzSasiedztwa[u][i]->waga + odleglosc[u] < odleglosc[v]) {
					odleglosc[v] = graph->macierzSasiedztwa[u][i]->waga + odleglosc[u];
					wierzcholki[v] = u;
					kopiec.decreaseKey(v, odleglosc[v]);
				}
			}
		}
	}

	printD(odleglosc, wierzcholki, V, s);
}