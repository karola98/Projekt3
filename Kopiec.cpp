#include"pch.h"
#include "Kopiec.h"

// zamiana dwóch krawędzi w kopcu
void swap(Edge *x, Edge *y)
{
	Edge temp = *x;
	*x = *y;
	*y = temp;
}

Kopiec::Kopiec(int pojemnosc)
{
	rozmiar = 0;
	maxRozmiar = pojemnosc;
	tablica = new Edge[pojemnosc];
}


void Kopiec::insertKey(Edge k)
{
	rozmiar++;
	int i = rozmiar - 1;
	tablica[i] = k;

	while (i != 0 && tablica[parent(i)].waga > tablica[i].waga)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		i = parent(i);
	}
}

void Kopiec::decreaseKey(int i, int nowa_waga)
{
	tablica[i].waga = nowa_waga;
	while (i != 0 && tablica[parent(i)].waga > tablica[i].waga)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		i = parent(i);
	}
}

Edge Kopiec::extractMin()
{
	if (rozmiar <= 0)
	{
		Edge temp;
		temp.waga = INT_MAX;
		return temp;
	}
	if (rozmiar == 1)
	{
		rozmiar--;
		return tablica[0];
	}

	Edge root = tablica[0];
	tablica[0] = tablica[rozmiar - 1];
	rozmiar--;
	heapify(0);

	return root;
}

void Kopiec::deleteKey(int i)
{
	decreaseKey(i, INT_MIN);
	extractMin();
}

void Kopiec::heapify(int i)
{
	int l = left(i);
	int r = right(i);
	int smallest = i;
	if (l < rozmiar && tablica[l].waga < tablica[i].waga)
		smallest = l;
	if (r < rozmiar && tablica[r].waga < tablica[smallest].waga)
		smallest = r;
	if (smallest != i)
	{
		swap(&tablica[i], &tablica[smallest]);
		heapify(smallest);
	}
}

