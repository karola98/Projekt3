#pragma once
#include "Graf.h"
#include "GrafM.h"
#include "KopiecP.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

void printPath(int parent[], int j);

void printD(int odleglosc[], int wierzcholki[], int n, int s);

void Dijkstra(Graf* graph, int s);

void Dijkstra(GrafM* graph, int s);