#pragma once
#include "GrafM.h"
#include "Graf.h"
#include "KopiecP.h"

Edge* Prim(GrafM* graph);
Edge* Prim(Graf* graph);