#pragma once
#include "kopiecNode.h"

// tworzenie nowego węzła kopca
kopiecNode* newNode(int v, int key);
// zamiana dwóch węzłów
void swapNode(kopiecNode** a, kopiecNode** b);

class KopiecP
{
public:
	KopiecP(int cap);
	~KopiecP();
	void heapify(int idx);
	bool isEmpty();
	kopiecNode* extractMin();
	void decreaseKey(int v, int key);
	bool areYouHere(int v);
	void insert(int v, int key);

	// Obecny rozmiar kopca
	int rozmiar;
	// maksymalny dopuszczalny rozmiar kopca
	int capacity;

	int* pos;
	kopiecNode** tablica;
};

