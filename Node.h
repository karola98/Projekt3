#pragma once
#include "pch.h"

template<class Typ>

class node {
public:
	// wska�nik na przechowywany element
	Typ element;
	// wska�nik na nast�pny w�ze�
	node *next;
	// wska�nik na poprzedni w�ze�
	node *prev;

	node()
	{
		next = nullptr;
		prev = nullptr;
	}
};
