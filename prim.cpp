#include "pch.h"
#include "prim.h"

void printP(Edge arr[], int n)
{
	for (int i = 1; i < n; ++i)
		std::cout << arr[i].src << "   ---   " << arr[i].waga << "   ---  " << arr[i].dst << "\n";
}

Edge * Prim(GrafM* graph)
{
	// liczba wierzchołków grafu
	int V = graph->V;
	// klucze wg których wybiera się kolejną krawędź
	int *key = new int[V];
	// wynik
	Edge *result = new Edge[V];
	// kopiec kluczy 
	KopiecP minHeap(V);

	// klucz pierwszego indeksu jest równy 0 
	key[0] = 0;
	minHeap.insert(0, key[0]);

	// Zapisanie kluczy w kopcu, na początku wszystkie klucze mają wartość INT_MAX
	for (int v = 1; v < V; ++v) {
		key[v] = INT_MAX;
		minHeap.insert(v, key[v]);
	}

	// dopóki nie zdejmiemy wszystkich wierzchołków 
	while (!minHeap.isEmpty()) {
		// Usuwanie wierzchołka o najmniejszym kluczu
		kopiecNode* minHeapNode = minHeap.extractMin();
		int u = minHeapNode->v;

		// Przejście przez wszystkie krawędzie przyległe do u 
		for (int i = 0; i < V; i++)
		{
			if (graph->macierzSasiedztwa[u][i] != nullptr)
			{
				// wierzchołek po drugiej stronie
				int v;
				if (graph->macierzSasiedztwa[u][i]->dst == u)
					v = graph->macierzSasiedztwa[u][i]->src;
				else
					v = graph->macierzSasiedztwa[u][i]->dst;

				// Jeśli wierzchołek v jest w kopcu a jego klucz jest większy od wagi krawędzi
				if (minHeap.areYouHere(v) && graph->macierzSasiedztwa[u][i]->waga < key[v]) {
					// Zmiana klucza wierzchołka v na wagę krawędzi
					key[v] = graph->macierzSasiedztwa[u][i]->waga;
					// dodanie krawędzi (u, v) do wyniku
					result[v] = *(graph->macierzSasiedztwa[u][i]);
					// zmiana wartości klucza v w kopcu
					minHeap.decreaseKey(v, key[v]);
				}
			}
		}
	}

	printP(result, V);
	return result;
}

Edge* Prim(Graf* graph)
{
	// liczba wierzchołków w grafie
	int V = graph->V;
	// klucze wg których wybiera się kolejną krawędź
	int *key = new int[V];
	// wynik
	Edge * result = new Edge[V];
	// kopiec kluczy 
	KopiecP minHeap(V);

	// klucz pierwszego indeksu jest równy 0 
	key[0] = 0;
	minHeap.insert(0, key[0]);

	// Zapisanie kluczy w kopcu, na początku wszystkie klucze mają wartość INT_MAX
	for (int v = 1; v < V; ++v) {
		key[v] = INT_MAX;
		minHeap.insert(v, key[v]);
	}

	// dopóki nie zdejmiemy wszystkich wierzchołków  
	while (!minHeap.isEmpty()) {
		// Usuwanie wierzchołka o najmniejszym kluczu
		kopiecNode* minHeapNode = minHeap.extractMin();
		int u = minHeapNode->v; // Store the extracted vertex number 

		// Przejście przez wszystkie krawędzie przyległe do u 
		graph->listaSasiedztwa[u].first();
		while (graph->listaSasiedztwa[u].wsk != NULL) {
			// wierzchołek po drugiej stronie
			int v;
			if (graph->listaSasiedztwa[u].get()->dst == u)
				v = graph->listaSasiedztwa[u].get()->src;
			else
				v = graph->listaSasiedztwa[u].get()->dst;


			// Jeśli wierzchołek v jest w kopcu a jego klucz jest większy od wagi krawędzi
			if (minHeap.areYouHere(v) && graph->listaSasiedztwa[u].get()->waga < key[v]) {
				// Zmiana klucza wierzchołka v na wagę krawędzi
				key[v] = graph->listaSasiedztwa[u].get()->waga;
				// dodanie krawędzi (u, v) do wyniku
				result[v] = *(graph->listaSasiedztwa[u].get());
				// zmiana wartości klucza v w kopcu
				minHeap.decreaseKey(v, key[v]);
			}
			graph->listaSasiedztwa[u].nextNode();
		}
	}

	printP(result, V);
	return result;
}