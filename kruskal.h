#pragma once
#include "Klaster.h"
#include "Graf.h"
#include "GrafM.h"
#include "Kopiec.h"

Edge* kruskal(Graf *g);
Edge* kruskal(GrafM *g);
int find(Klaster k[], int i);
void link(Klaster k[], int x, int y);
void merge(Klaster k[], int x, int y);