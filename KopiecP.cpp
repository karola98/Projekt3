#include "pch.h"
#include "KopiecP.h"

// Tworzenie nowego węzła kopca
kopiecNode* newNode(int v, int key)
{
	kopiecNode* nowy = new kopiecNode;
	nowy->v = v;
	nowy->key = key;
	return nowy;
}

// Zamiana dwóch węzłów kopca
void swapNode(kopiecNode** a, kopiecNode** b)
{
	kopiecNode* t = *a;
	*a = *b;
	*b = t;
}

KopiecP::KopiecP(int cap)
{
	pos = new int[cap];
	rozmiar = 0;
	capacity = cap;
	tablica = new kopiecNode*[cap];
}


KopiecP::~KopiecP()
{
	delete[] tablica;
	delete pos;
}

void KopiecP::heapify(int i)
{
	int smallest, left, right;
	smallest = i;
	left = 2 * i + 1;
	right = 2 * i + 2;

	if (left < rozmiar && tablica[left]->key < tablica[smallest]->key)
		smallest = left;

	if (right < rozmiar && tablica[right]->key < tablica[smallest]->key)
		smallest = right;

	if (smallest != i) {
		kopiecNode* smallestNode = tablica[smallest];
		kopiecNode* idxNode = tablica[i];

		pos[smallestNode->v] = i;
		pos[idxNode->v] = smallest;

		swapNode(&tablica[smallest], &tablica[i]);

		heapify(smallest);
	}
}

bool KopiecP::isEmpty()
{
	if (rozmiar == 0)
		return true;
	else
		return false;
}


kopiecNode* KopiecP::extractMin()
{
	if (isEmpty())
		return nullptr;

	struct kopiecNode* root = tablica[0];
	struct kopiecNode* lastNode = tablica[rozmiar - 1];
	tablica[0] = lastNode;

	pos[root->v] = rozmiar - 1;
	pos[lastNode->v] = 0;

	--rozmiar;
	heapify(0);

	return root;
}

void KopiecP::decreaseKey(int v, int key)
{

	int i = pos[v];

	tablica[i]->key = key;

	while (i && tablica[i]->key < tablica[(i - 1) / 2]->key) {
		pos[tablica[i]->v] = (i - 1) / 2;
		pos[tablica[(i - 1) / 2]->v] = i;
		swapNode(&tablica[i], &tablica[(i - 1) / 2]);
		i = (i - 1) / 2;
	}
}


bool KopiecP::areYouHere(int v)
{
	if (pos[v] < rozmiar)
		return true;
	return false;
}

void KopiecP::insert(int v, int key)
{
	tablica[v] = newNode(v, key);
	pos[v] = v;
	rozmiar++;
}