﻿// Projekt3c.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//
#include "pch.h"
#include "Vertex.h"
#include "Edge.h"
#include "Lista.h"
#include "Graf.h"
#include <iostream>
#include "GrafM.h"
#include "Kopiec.h"
#include "Klaster.h"
#include "kruskal.h"
#include "prim.h"
#include "Dijkstra.h"
#include "Bellman.h"
#include "testy.h"
#include <fstream>
#include <string>
#include <map>
#include <random>

void printKruskal(Edge tablica[], int e)
{
	std::cout << "Drzewo MST\n";
	for (int i = 0; i < e; i++)
	{
		std::cout << tablica[i].src << "   ---   " << tablica[i].waga << "   ---   " << tablica[i].dst << "\n";
	}
}



int main()
{
	std::fstream plik;
	plik.open("graphV10Dens0.5.txt", std::ios::in);
	
	GrafM graf(plik);
	//graf.print();

	Bellman(&graf, 9);
	/*std::fstream zapis;
	zapis.open("1.txt", std::fstream::app);
	std::fstream zapisM;
	zapisM.open("2.txt", std::fstream::app);

	int V = 1000;
	int E = 499500;

	Graf graph(V);
	GrafM graphM(V);
	
	for (int i = 0; i < 50; i++)
	{
		for (int i = 0; i < V; i++)
		{
			graph.insertVertex(i);
			graphM.insertVertex(i);
		}

		while (graph.krawedzie.size != E)
		{
			std::random_device dev;
			std::mt19937 rng(dev());
			std::uniform_int<uint32_t> vertex_distribution(0, V - 1);

			int from = vertex_distribution(rng);
			int to = vertex_distribution(rng);

			std::uniform_int_distribution<uint32_t> weight_distribution(0, 100);
			int cost = weight_distribution(rng);

			bool *S = new bool[V];
			bool *D = new bool[V];

			for (int i = 0; i < V; i++)
			{
				S[i] = false;
				D[i] = false;
			}

			if (!(S[from] == true && D[to] == true))
			{
				if (from != to)
				{
					graph.insertEdge(from, to, cost);
					graphM.insertEdge(from, to, cost);
					S[from] = true;
					D[to] = true;
				}
			}
		}

		auto start = std::chrono::system_clock::now();
		kruskal(&graph);
		auto end = std::chrono::system_clock::now();

		std::chrono::duration<double> diff = end - start;
		double durationTime = diff.count();
		zapis << durationTime << "\n";

		auto startM = std::chrono::system_clock::now();
		kruskal(&graphM);
		auto endM = std::chrono::system_clock::now();

		std::chrono::duration<double> diffM = endM - startM;
		double durationTimeM = diffM.count();
		zapisM << durationTimeM << "\n";

	}*/
}
// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

	/*	graph.insertVertex(0);
		graph.insertVertex(1);
		graph.insertVertex(2);
		graph.insertVertex(3);
		graph.insertVertex(4);
		graph.insertVertex(5);
		graph.insertVertex(6);
		graph.insertVertex(7);
		graph.insertVertex(8);
		graph.insertEdge(0, 1, 4);
		graph.insertEdge(0, 7, 8);
		graph.insertEdge(1, 2, 8);
		graph.insertEdge(1, 7, 11);
		graph.insertEdge(2, 3, 7);
		graph.insertEdge(2, 8, 2);
		graph.insertEdge(2, 5, 4);
		graph.insertEdge(3, 4, 9);
		graph.insertEdge(3, 5, 14);
		graph.insertEdge(4, 5, 10);
		graph.insertEdge(5, 6, 2);
		graph.insertEdge(6, 7, 1);
		graph.insertEdge(6, 8, 6);
		graph.insertEdge(7, 8, 7);
		graph.insertEdge(1, 8, 1);*/
		//graph.print();

		//Edge * tab = new Edge[V];
		//Bellman(&graph, 0);