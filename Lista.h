#pragma once
#include "pch.h"
#include "Node.h"
#include <iostream>

template <class Typ>
class Lista
{
public:
	// wska�nik na pierwszy w�ze�
	node<Typ> *header;
	// wska�nik na ostatni w�ze�
	node<Typ> *tailer;
	// wska�nik na aktualnie u�ywany w�ze�
	node<Typ> *wsk;

	// ilo�� w�z��w w li�cie
	int size = 0;

	// funkcja zwraca wska�nik do wcze�niejszego w�z�a
	node<Typ>* prev_node(node<Typ> *ref);

	Lista()
	{
		header = nullptr;
		tailer = nullptr;
		wsk = nullptr;
	}
	~Lista();

	// Funkcja zwraca ilo�� w�z��w na li�cie
	int get_size() const
	{
		return size;
	}
	node<Typ>* getNode()
	{
		return wsk;
	}

	Typ get();
	Typ& set();
	Typ value();

	// Funkcja zwraca pierwszy element listy 
	Typ front() const;
	// Funkcja ustawia wsk na pierwszy w�ze� listy
	void first();


	// Funkcja zwraca ostatni element listy
	Typ back() const;

	Typ& ostatni();
	// Funkcja ustawia wsk na ostatni w�ze� listy
	void last();

	// Funkcja w�ze� wskazuj�cy na Element na pocz�tek listy
	void push_front(Typ Element);
	// Funkcja w�ze� wskazuj�cy na Element na koniec listy
	void push_back(Typ Element);

	void nextNode()
	{
		if(wsk!=nullptr)
			wsk = wsk->next;
	}

	// Funkcja wy�wietla wszystkie elementy listy
	void wyswietl_wszystkie() const;

	// Funkcja usuwa ostatni w�ze� z listy
	void pop_back();
	// Funkcja usuwa piewszy w�ze� z listy
	void pop_front();

	// usuwa ze �rodka
	void pop();

	// Funkcja usuwa wszystkie elementy
	void usun_wszystkie();

	// Funkcja sprawdza czy lista jest pusta
	bool isEmpty() const
	{
		if (header == nullptr)
			return true;
		else
			return false;
	}

	Typ getValue(int indeks) const;
	node<Typ>* getWsk(int indeks) const;
	Lista<Typ> & operator = (const Lista<Typ> &lista);

	Typ* find(Typ ref);

};

template <class Typ>
Lista<Typ>::~Lista()
{
}

template <class Typ>
Typ Lista<Typ>::get()
{
	return wsk->element;
}

template <class Typ>
Typ& Lista<Typ>::set()
{
	return wsk->element;
}

template <class Typ>
Typ Lista<Typ>::front() const
{
	return header->element;
}

template <class Typ>
Typ Lista<Typ>::value()
{
	return (wsk->element);
}

template <class Typ>
void Lista<Typ>::first()
{
	wsk = header;
}

template <class Typ>
Typ Lista<Typ>::back() const
{
	return tailer->element;
}

template <class Typ>
Typ& Lista<Typ>::ostatni()
{
	return tailer->element;
}

template <class Typ>
void Lista<Typ>::last()
{
	wsk = tailer;
}

template <class Typ>
node<Typ>* Lista<Typ>::prev_node(node<Typ> *ref)
{
	return ref->prev;
}

template <class Typ>
void Lista<Typ>::push_front(Typ Element)
{
	first();
	node<Typ> *nowy = new node<Typ>;
	nowy->element = Element;
	++size;

	if (!isEmpty())
	{
		nowy->next = wsk;
		nowy->prev = nullptr;
		wsk->prev = nowy;
		header = nowy;
		wsk = nowy;
	}
	else
	{
		header = nowy;
		tailer = nowy;
		wsk = nullptr;
	}
}

template <class Typ>
void Lista<Typ>::push_back(Typ Element)
{
	last();
	node<Typ> *nowy = new node<Typ>;
	nowy->element = Element;
	++size;

	if (!isEmpty())
	{
		tailer->next = nowy;
		nowy->prev = tailer;
		tailer = nowy;
		wsk = nullptr;
	}
	else
	{
		header = nowy;
		tailer = nowy;
		wsk = nullptr;
	}
}


template <class Typ>
void Lista<Typ>::pop_back()
{
	last();
	--size;

	try
	{
		if (!isEmpty())
		{
			if (header->next) //usuwanie je�li jest wi�cej element�w ni� tylko startowy
			{
				tailer = tailer->prev;
				tailer->next = nullptr;
				delete wsk; //usuwamy ostatniego
				wsk = tailer; //oznaczamy nowy koniec listy
			}
			else //by� na li�cie tylko startowy
			{
				delete wsk;
				header = nullptr;
				tailer = nullptr;
			}
		}
		else
			throw "pusto";
	}

	catch (...)
	{
		std::cout << "Lista jest pusta!\n";
	}
}

template <class Typ>
void Lista<Typ>::pop_front()
{
	first();

	try
	{
		if (!isEmpty())
		{
			if (header->next)
			{
				--size;
				header = header->next;
				delete wsk;
				first();
				header->prev = nullptr;
			}
			else
			{
				--size;
				delete wsk;
				header->prev = nullptr;
			}
		}
		else
			throw "pusto";
	}

	catch (...)
	{
		std::cout << "Lista jest pusta!\n";
	}
}

template <class Typ>
void Lista<Typ>::usun_wszystkie()
{
	int n = size;
	for (int i = 0; i < n; i++)
		pop_front();
}

template <class Typ>
void Lista<Typ>::wyswietl_wszystkie() const
{
	node<Typ> *i = header;

	std::cout << "Cala lista: ";

	while (i)
	{
		std::cout << (i->element) << "  ";
		i = i->next;
	}
	std::cout << "\n";
}

template <class Typ>
Typ Lista<Typ>::getValue(int indeks) const
{
	node<Typ> *i = header;
	int j = 0;
	while (j < indeks)
	{
		j++;
		i = i->next;
	}

	return (i->element);
}

template <class Typ>
node<Typ>* Lista<Typ>::getWsk(int indeks) const
{
	node<Typ> *i = header;
	int j = 0;
	while (j < indeks)
	{
		j++;
		i = i->next;
	}

	return (i->element);
}

template <class Typ>
Lista<Typ> & Lista<Typ>::operator = (const Lista<Typ> &lista)
{

	int m = lista.get_size();

	usun_wszystkie();


	Typ * tablica = new Typ[m];
	for (int i = 0; i < m; i++)
	{
		tablica[i] = lista.getValue(i);
		push_back(tablica[i]);

	}

	//wyswietl_wszystkie();

	return *this;
}

template <class Typ>
Typ* Lista<Typ>::find(Typ ref)
{
	first();
	if (!isEmpty())
	{
		while (wsk->element != ref)
		{
			nextNode();
		}
	}
}

template <class Typ>
void Lista<Typ>::pop()
{
	if (!wsk) return;
	if (wsk == header)
	{
		pop_front();
	}
	else
	{
		if (wsk == tailer)
		{
			pop_back();
		}
		else
		{
			--size;
			node<Typ>* ptr = wsk->prev;

			ptr->next = wsk->next;
			(wsk->next)->prev = ptr;
			delete wsk;
			wsk = ptr;
		}
	}
}
