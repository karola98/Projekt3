#pragma once
#include "pch.h"
#include "Edge.h"
#include <iostream>


class Kopiec
{
public:
	Edge *tablica; 
	// maksymalny rozmiar kopca
	int maxRozmiar; 
	// aktualna ilość elementów w kopcu
	int rozmiar; 

	Kopiec(int pojemnosc);

	void heapify(int);

	int parent(int i) { return (i - 1) / 2; }
	int left(int i) { return (2 * i + 1); }
	int right(int i) { return (2 * i + 2); }

	Edge extractMin();

	void decreaseKey(int i, int nowa_waga);

	// zwraca korzeń
	Edge getMin() { return tablica[0]; }

	// usuwa element z indeksem i
	void deleteKey(int i);

	// nowy element
	void insertKey(Edge k);

	bool isEmpty()
	{
		if (rozmiar == 0)
			return true;
		else
			return false;
	}
};

