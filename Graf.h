#pragma once
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include "Lista.h"
#include "Vertex.h"
#include "Edge.h"

class Graf
{
public:
	Graf(int rozmiar);
	Graf(std::fstream &plik);
	~Graf();

	void insertVertex(int element);
	Vertex* findVer(int ver);
	Edge* findEdge(int edge);
	void insertEdge(int ver1, int ver2, int wartosc);
	Vertex * endVertices(int wagaEdge);
	Vertex * opposite(int v, int e);
	int oppositeValue(int v, int e);
	void replace(int ver, int x);

	void removeVertex(int v);
	void removeEdge(int v);

	void print();

	bool areAdjacent(int ver1, int ver2);
	// liczba wierzcholkow
	int V;
	// lista wierzcholkow
	Lista<Vertex> wierzcholki;

	Lista<Edge*> * listaSasiedztwa;
	// lista krawedzi
	Lista<Edge> krawedzie;

	// liczba wierzchołków 
	static int liczbaInstancji;
};

