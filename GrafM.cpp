#include "pch.h"
#include "GrafM.h"

int GrafM::liczbaInstancji = 0;

GrafM::GrafM(int rozmiar)
{
	V = rozmiar;
	macierzSasiedztwa = new Edge**[V]; //alokacja pamieci
	for (int i = 0; i < V; ++i)
	{
		macierzSasiedztwa[i] = new Edge*[V]; //alokacja pamieci
		for (int j = 0; j < V; ++j) //wpisanie wartosci do tablicy
			macierzSasiedztwa[i][j] = nullptr;
	}
}

GrafM::GrafM(std::fstream &plik)
{
	int E;
	int from, to, waga;

	if (plik.good() == true)
	{
		plik >> V >> E;
		
		macierzSasiedztwa = new Edge**[V]; //alokacja pamieci
		for (int i = 0; i < V; ++i)
		{
			macierzSasiedztwa[i] = new Edge*[V]; //alokacja pamieci
			for (int j = 0; j < V; ++j) //wpisanie wartosci do tablicy
				macierzSasiedztwa[i][j] = nullptr;
		}

		for (int i = 0; i < V; i++)
		{
			insertVertex(i);
		}

		for (int e = 0; e < E; e++)
		{
			plik >> from >> to >> waga;
			insertEdge(from, to, waga);
		}
	}
}

GrafM::~GrafM()
{
	delete[] macierzSasiedztwa;
}


void GrafM::insertVertex(int element)
{
	Vertex ver;
	wierzcholki.push_back(ver);
	wierzcholki.ostatni().wartosc = element;
	wierzcholki.ostatni().indeks = liczbaInstancji;
	//	std::cout << "\nindeks po dodawniu " << wierzcholki.back().indeks << "      wartosc:" << wierzcholki.back().wartosc;
	++liczbaInstancji;
}

Vertex* GrafM::findVer(int ver)
{
	wierzcholki.first();

	while (wierzcholki.getNode() != nullptr && wierzcholki.getNode()->element.wartosc != ver)
	{
		wierzcholki.nextNode();
	}

	if (wierzcholki.getNode() == nullptr)
		return nullptr;
	else
		return &wierzcholki.getNode()->element;

}

Edge* GrafM::findEdge(int edge)
{
	krawedzie.first();

	while (krawedzie.getNode() != nullptr && krawedzie.getNode()->element.waga != edge)
	{
		krawedzie.nextNode();
	}

	if (krawedzie.getNode() == nullptr)
		return nullptr;
	else
		return &krawedzie.getNode()->element;
}

void GrafM::insertEdge(int ver1, int ver2, int wartosc)
{
	Vertex* verA = findVer(ver1);;
	Vertex* verB = findVer(ver2);

	int i = verA->indeks;
	int j = verB->indeks;
	Edge nowy(verA, verB, wartosc);

	krawedzie.push_back(nowy);
	macierzSasiedztwa[i][j] = &krawedzie.ostatni();

	macierzSasiedztwa[j][i] = &krawedzie.ostatni();
}

bool GrafM::areAdjacent(int ver1, int ver2)
{
	Vertex* verA = findVer(ver1);
	Vertex* verB = findVer(ver2);

	if (verA == nullptr || verB == nullptr)
		return false;

	//std::cout << "\n" << "A" << verA->wartosc << "   " << verA->indeks << "\n" << "B" << verB->wartosc << "   " << verB->indeks;

	Edge *e;

	int i = verA->indeks;
	int j = verB->indeks;

	if (macierzSasiedztwa[i][j] == nullptr)
		return false;
	
	return true;
}

Vertex * GrafM::endVertices(int wagaEdge)
{
	Edge * e = findEdge(wagaEdge);

	Vertex * v = new Vertex[2];

	v[0] = *(e->wierzcholekA);
	v[1] = *(e->wierzcholekB);

	return v;
}


Vertex * GrafM::opposite(int v, int e)
{
	Vertex * ver = findVer(v);

	int i = 0;
	while (i<V)
	{
		if (macierzSasiedztwa[ver->indeks][i] != nullptr)
		{
			if (macierzSasiedztwa[ver->indeks][i]->waga == e)
				break;
		}
		i++;
	}
	
	
	if (macierzSasiedztwa[ver->indeks][i]->wierzcholekA->wartosc == v)
		return macierzSasiedztwa[ver->indeks][i]->wierzcholekB;
	else
		return macierzSasiedztwa[ver->indeks][i]->wierzcholekA;

}

int GrafM::oppositeValue(int v, int e)
{
	return opposite(v, e)->wartosc;
}

void GrafM::replace(int v, int x)
{
	Vertex * ver = findVer(v);

	ver->wartosc = x;
}

void GrafM::removeVertex(int v)
{
	Vertex *ver = findVer(v);
	
	for (int i = 0; i < V; i++)
	{
		if (macierzSasiedztwa[ver->indeks][i] != nullptr)
			macierzSasiedztwa[ver->indeks][i] = nullptr;

		if (macierzSasiedztwa[i][ver->indeks] != nullptr)
		{
			findEdge(macierzSasiedztwa[i][ver->indeks]->waga);
			macierzSasiedztwa[i][ver->indeks] = nullptr;
			krawedzie.pop();
		}
	}

	wierzcholki.pop();
}

void GrafM::removeEdge(int e)
{
	Edge *edge = findEdge(e);
	Vertex temp = (endVertices(e))[0];
	Vertex temp1 = (endVertices(e))[1];

	macierzSasiedztwa[temp.indeks][temp1.indeks] = nullptr;
	macierzSasiedztwa[temp1.indeks][temp.indeks] = nullptr;
	krawedzie.pop();
}

void GrafM::print()
{
	std::cout << "***** GRAF *****\n";
	wierzcholki.first();

	for (int i = 0; i < wierzcholki.size; i++)
	{
		std::cout << "Wierzcholek       "  << wierzcholki.get().wartosc  << "\n";
		for (int j = 0; j < V; j++)
		{
			if (macierzSasiedztwa[wierzcholki.get().indeks][j] == nullptr)
				std::cout << wierzcholki.get().wartosc << "\n";
			else
			{
				std::cout << wierzcholki.get().wartosc << "----  " << macierzSasiedztwa[wierzcholki.get().indeks][j]->waga << "---- " << oppositeValue(wierzcholki.get().wartosc, macierzSasiedztwa[wierzcholki.get().indeks][j]->waga);
				std::cout << "\n";
			}
		}
		wierzcholki.nextNode();
	}

}