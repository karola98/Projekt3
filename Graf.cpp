#include "pch.h"
#include "Graf.h"


int Graf::liczbaInstancji = 0;

Graf::Graf(int rozmiar)
{
	V = rozmiar;
	listaSasiedztwa = new Lista<Edge*>[V];
}

Graf::Graf(std::fstream &plik)
{
	int E;
	int from, to, waga;

	if (plik.good() == true)
	{
		plik >> V >> E;
		listaSasiedztwa = new Lista<Edge*>[V];
		for (int i = 0; i < V; i++)
		{
			insertVertex(i);
		}
		
		for (int e = 0; e < E; e++)
		{
			plik >> from >> to >> waga;
			insertEdge(from, to, waga);	
		}
	}
}

Graf::~Graf()
{
	delete[] listaSasiedztwa;
}

void Graf::insertVertex(int element)
{
	Vertex ver;
	wierzcholki.push_back(ver);
	wierzcholki.ostatni().wartosc = element;
	wierzcholki.ostatni().indeks = liczbaInstancji;
	//	std::cout << "\nindeks po dodawniu " << wierzcholki.back().indeks << "      wartosc:" << wierzcholki.back().wartosc;
	++liczbaInstancji;
}

Vertex* Graf::findVer(int ver)
{
	wierzcholki.first();

	while (wierzcholki.getNode() != nullptr && wierzcholki.getNode()->element.wartosc != ver)
	{
		wierzcholki.nextNode();
	}

	if (wierzcholki.getNode() == nullptr)
		return nullptr;
	else
		return &wierzcholki.getNode()->element;

}

Edge* Graf::findEdge(int edge)
{
	krawedzie.first();

	while (krawedzie.getNode() != nullptr && krawedzie.getNode()->element.waga != edge)
	{
		krawedzie.nextNode();
	}

	if (krawedzie.getNode() == nullptr)
		return nullptr;
	else
		return &krawedzie.getNode()->element;

}

void Graf::insertEdge(int ver1, int ver2, int wartosc)
{
	Vertex* verA = findVer(ver1);;
	Vertex* verB = findVer(ver2);

	int i = verA->indeks;
	int j = verB->indeks;
	Edge nowy(verA, verB, wartosc);

	krawedzie.push_back(nowy);

	/*Edge tempA;
	tempA.wierzcholekA = verA;
	tempA.wierzcholekB = verB;
	tempA.src = ver1;
	tempA.dst = ver2;
	tempA.waga = wartosc;*/
	listaSasiedztwa[i].push_back(&krawedzie.ostatni());

	/*Edge tempB;
	tempB.wierzcholekA = verB;
	tempB.wierzcholekB = verA;
	tempB.src = ver2;
	tempB.dst = ver1;
	tempB.waga = wartosc;*/
	listaSasiedztwa[j].push_back(&krawedzie.ostatni());
}


bool Graf::areAdjacent(int ver1, int ver2)
{
	Vertex* verA = findVer(ver1);
	Vertex* verB = findVer(ver2);

	if (verA == nullptr || verB == nullptr)
		return false;

	std::cout << "\n" << "A" << verA->wartosc << "   " << verA->indeks << "\n" << "B" << verB->wartosc << "   " << verB->indeks;


	int i = verA->indeks;
	int j = verB->indeks;

	listaSasiedztwa[i].first();

	while (listaSasiedztwa[i].wsk != nullptr)
	{
		if (listaSasiedztwa[i].get()->areYouHere(ver1, ver2))
			return true;

		listaSasiedztwa[i].nextNode();
	}

	return false;
}


Vertex * Graf::endVertices(int wagaEdge)
{
	Edge * e = findEdge(wagaEdge);

	Vertex * v = new Vertex[2];

	v[0] = *(e->wierzcholekA);
	v[1] = *(e->wierzcholekB);

	return v;
}

Vertex * Graf::opposite(int v, int e)
{
	Vertex * ver = findVer(v);

	listaSasiedztwa[ver->indeks].first();
	while (listaSasiedztwa[ver->indeks].get()->waga != e)
	{
		listaSasiedztwa[ver->indeks].nextNode();
	}

	if (listaSasiedztwa[ver->indeks].get()->wierzcholekA->wartosc == v)
		return listaSasiedztwa[ver->indeks].get()->wierzcholekB;
	else
		return listaSasiedztwa[ver->indeks].get()->wierzcholekA;

}

int Graf::oppositeValue(int v, int e)
{
	return opposite(v, e)->wartosc;
}

void Graf::replace(int v, int x)
{
	Vertex * ver = findVer(v);

	ver->wartosc = x;
}

void Graf::removeVertex(int v)
{
	Vertex *ver = findVer(v);
	int n = listaSasiedztwa[ver->indeks].size;
	int *tab = new int[n];

	listaSasiedztwa[ver->indeks].first();

	for (int i = 0; i < listaSasiedztwa[ver->indeks].size; i++)
	{
		tab[i] = listaSasiedztwa[ver->indeks].get()->waga;

		listaSasiedztwa[ver->indeks].nextNode();
	}

	for (int i = 0; i < n; i++)
	{
		removeEdge(tab[i]);
	}

	/*
	for (int i = 0; i < listaSasiedztwa[ver->indeks].size; i++)
	{
		listaSasiedztwa[ver->indeks].first();
		temp1 = findEdge(listaSasiedztwa[ver->indeks].get()->waga);
		temp = opposite(ver->wartosc, listaSasiedztwa[ver->indeks].get()->waga);

		listaSasiedztwa[ver->indeks].pop();

		while (listaSasiedztwa[temp->indeks].get()->waga != temp1->waga)
		{
			listaSasiedztwa[temp->indeks].nextNode();
		}

		listaSasiedztwa[temp->indeks].pop();
		krawedzie.pop();
		listaSasiedztwa[ver->indeks].nextNode();

	}
	*/
	//	listaSasiedztwa[ver->indeks].usun_wszystkie();
	wierzcholki.pop();
	delete[]tab;
}

void Graf::removeEdge(int e)
{
	Edge *edge = findEdge(e);
	Vertex temp = endVertices(e)[0];
	Vertex temp1 = endVertices(e)[1];

	listaSasiedztwa[temp.indeks].first();
	while (listaSasiedztwa[temp.indeks].isEmpty() == false && listaSasiedztwa[temp.indeks].get()->waga != e)
		listaSasiedztwa[temp.indeks].nextNode();
	listaSasiedztwa[temp.indeks].pop();

	listaSasiedztwa[temp1.indeks].first();
	while (listaSasiedztwa[temp1.indeks].isEmpty() == false && listaSasiedztwa[temp1.indeks].get()->waga != e)
		listaSasiedztwa[temp1.indeks].nextNode();
	listaSasiedztwa[temp1.indeks].pop();

	krawedzie.pop();
}

void Graf::print()
{
	wierzcholki.first();
	for (int i = 0; i < wierzcholki.size; i++)
	{
		std::cout << "Wierzcholek     " << wierzcholki.get().wartosc << "\n";
		if (listaSasiedztwa[wierzcholki.get().indeks].isEmpty() == false)
			//			std::cout << wierzcholki.get().wartosc;
				//	else
		{
			listaSasiedztwa[wierzcholki.get().indeks].first();
			for (int j = 0; j < listaSasiedztwa[wierzcholki.get().indeks].size; j++)
			{
				std::cout << wierzcholki.get().wartosc << "----" << listaSasiedztwa[wierzcholki.get().indeks].get()->waga << "----" << oppositeValue(wierzcholki.get().wartosc, listaSasiedztwa[wierzcholki.get().indeks].get()->waga) << "\n";
				listaSasiedztwa[wierzcholki.get().indeks].nextNode();
			}
		}
		wierzcholki.nextNode();
	}

}

