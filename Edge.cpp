#include "pch.h"
#include "Edge.h"


Edge::Edge()
{
}

Edge::Edge(Vertex* ver1, Vertex* ver2, int wartosc)
{
	src = ver1->wartosc;
	dst = ver2->wartosc;
	wierzcholekA = ver1;
	wierzcholekB = ver2;

	waga = wartosc;
	
}

Edge::~Edge()
{
}

bool Edge::areYouHere(int ver1, int ver2)
{
	if (ver1 == wierzcholekA->wartosc && ver2 == wierzcholekB->wartosc)
		return true;
	if (ver1 == wierzcholekB->wartosc && ver2 == wierzcholekA->wartosc)
		return true;

	return false;
}
Edge & Edge::operator = (const Edge &krawedz)
{
	wierzcholekA = krawedz.wierzcholekA;
	wierzcholekB = krawedz.wierzcholekB;
	dst = krawedz.dst;
	src = krawedz.src;
	waga = krawedz.waga;

	return *this;
}