#pragma once
#include "Edge.h"
#include "Vertex.h"
#include "Lista.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>


class GrafM
{
public:
	GrafM(int rozmiar);
	GrafM(std::fstream &plik);
	~GrafM();

	void insertVertex(int element);
	Vertex* findVer(int ver);
	Edge* findEdge(int edge);
	void insertEdge(int ver1, int ver2, int wartosc);
	bool areAdjacent(int ver1, int ver2);
	Vertex * endVertices(int wagaEdge);
	Vertex * opposite(int v, int e);
	int oppositeValue(int v, int e);
	void replace(int v, int x);
	void removeVertex(int v);
	void removeEdge(int e);
	void print();

	Lista<Vertex> wierzcholki;

	Edge ***macierzSasiedztwa;
	// lista krawedzi
	Lista<Edge> krawedzie;

	// liczba wierzchołków 
	static int liczbaInstancji;

	int V;
};

